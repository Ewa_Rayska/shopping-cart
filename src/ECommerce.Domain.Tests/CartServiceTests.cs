using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Domain.Tests
{
    public class CartServiceTests
    {
        private List<Cart> carts;
        CartService cartService;

        [SetUp]
        public void Setup()
        {
            carts = new List<Cart>();
            cartService = new CartService(carts);
        }

        [Test]
        public void WhenConstructorCalledWithNullList_ThenThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                new CartService(null);
            });
        }

        [Test]
        public void WhenConstructorCalledWithEmptyList_ThenSucceeds()
        {
            Assert.DoesNotThrow(() =>
            {
                new CartService(new List<Cart>());
            });
        }

        [TestCaseSource(nameof(NonEmptyLists))]
        public void WhenConstructorCalledWithNotEmptyList_ThenSucceeds(List<Cart> carts)
        {
            Assert.DoesNotThrow(() =>
            {
                new CartService(carts);
            });
        }

        public static IEnumerable<List<Cart>> NonEmptyLists
        {
            get
            {
                return new[]
                {
                    new List<Cart> { new Cart(10) },
                    new List<Cart> { new Cart(10), new Cart(11) }
                };
            }
        }

        [Test]
        public void WhenInitializingNewCart_ThenCartShouldNotBeNullAndHaveIdDefined()
        {
            // Arrange/Given

            // Act/When
            var cartId = cartService.Init();

            // Assert/Then
            Assert.IsNotNull(cartId);
            Assert.AreNotEqual(0, cartId);

            CollectionAssert.IsNotEmpty(carts);
            Assert.AreEqual(1, carts.Count);

            var cartFromList = carts.Single();

            Assert.AreEqual(cartId, cartFromList.Id);
            CollectionAssert.IsEmpty(cartFromList.Products);
        }

        [Test]
        public void GetById_ForExistingId_ReturnsNonEmptyCart()
        {
            //Arange
            var existingCart = new Cart(1);
            cartService = new CartService(new List<Cart> { existingCart });

            //Act
            var cart = cartService.GetById(1);

            //Asser
            Assert.IsNotNull(cart);
            Assert.AreEqual(existingCart, cart);
        }

        [Test]
        public void GetById_ForZero_ThrowsArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                cartService.GetById(0);
            });
        }

        [TestCaseSource(nameof(ListForNotExistingId))]
        public void GetById_ForNotExistingId_ReturnNull(List<Cart> carts)
        {
            //Arange
            cartService = new CartService(carts);

            //Act
            var cart = cartService.GetById(1);

            //Assert
            Assert.IsNull(cart);
        }
        public static IEnumerable<List<Cart>> ListForNotExistingId
        {
            get
            {
                return new[]
                {
                    new List<Cart> (),
                    new List<Cart> { new Cart(10), new Cart(11) }
                };
            }
        }
    }
}