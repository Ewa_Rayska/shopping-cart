﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Domain
{
    public class CartService
    {
        List<Cart> carts;

        public CartService(List<Cart> carts)
        {
            this.carts = carts ?? throw new ArgumentNullException(nameof(carts));
        }

        public int Init()
        {
            int id = new Random().Next(0, int.MaxValue);
            var cart = new Cart(id);
            carts.Add(cart);
            return cart.Id;
        }
        public Cart GetById(uint id)
        {
            if(id == 0) throw new ArgumentOutOfRangeException(nameof(id));

            return carts.FirstOrDefault(c => c.Id == id);
        }
    }
}
