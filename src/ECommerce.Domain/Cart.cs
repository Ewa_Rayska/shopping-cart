﻿namespace ECommerce.Domain;

public class Cart
{
    public int Id { get; set; }

    public List<Product> Products { get; set; }

    public Cart(int id)
    {
        Id = id;
        Products = new List<Product>();
    }

}