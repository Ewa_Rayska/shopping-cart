﻿namespace ECommerce.Domain
{
    public class Client
    {
        public int Id { get; set; }
        public Cart Cart { get; set; }
    }
}
