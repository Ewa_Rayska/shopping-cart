﻿using ECommerce.Domain;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShoppingCartController : Controller
    {
        CartService cartService;
        public ShoppingCartController(CartService cartService)
        {
            this.cartService = cartService;
        }

        [HttpPost]
        public IActionResult InitCart()
        {
            var cartId = cartService.Init();
            return Created($"/{cartId}/", new {id = cartId});

        }

        [HttpGet("{id}")]
        public IActionResult GetCart([FromRoute]uint id)
        {    
            return Ok(cartService.GetById(id));
        }

        //[HttpGet]
        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
